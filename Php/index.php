<?php 
include 'includes/Encrypt.php';

$key_base64 = 'auH8xLEXjZ4h3QbEFjkp+A=='; //La misma que en Java en base 64.
$key = base64_decode($key_base64);//Hay que decodificar  la llave que esta en base 64
$super_secret_message = ':v';
$cipher = new Encrypt();
$encrypted_message_base64 = $cipher->encryptData($key,$super_secret_message);
$decrypted_message = $cipher->decryptData($key, $encrypted_message_base64);
echo "Mensaje sin encriptar:";
echo "<br>";
echo "<strong>$super_secret_message</strong>";
echo "<br>";
echo "Mensaje encriptado y pasado a base64 junto con su iv respectivo:";
echo "<br>";
echo '<strong>'.$encrypted_message_base64.'</strong>';
echo "<br>";
echo "Mensaje desencriptado:";
echo "<br>";
echo '<strong>'.$decrypted_message.'</strong>';