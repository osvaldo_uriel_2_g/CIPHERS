<?php 
/**
Se requiere de PHP 7 o superior, se puede implementar desde PHP 5 sin embargo la función generate_password no funcionara.
 * 
 */
class Encrypt {

    private $OPENSSL_CIPHER_NAME = "aes-128-cbc"; //Nombre del cifrado 
    private $CIPHER_KEY_LEN; //128 bits
    /**
     * Encriptar datos usando el cifrado AES Cipher (CBC) con un llave de 128 bits
     * 
     * @param type $key - La llave debe ser de una longitud exacta de 16 bytes (128 bits)
     * @param type $data - Datos a encriptar
     * @return Datos encriptados y pasados a base64 junto con el IV separados por :
     */
    function __construct(){
        $this->CIPHER_KEY_LEN = openssl_cipher_iv_length($this->OPENSSL_CIPHER_NAME);
    }
    public function encryptData($key,$data) {
    	if (strlen($key) == $this->CIPHER_KEY_LEN) {
    		$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->OPENSSL_CIPHER_NAME));
	        $encodedEncryptedData = base64_encode(openssl_encrypt($data, $this->OPENSSL_CIPHER_NAME, $key, OPENSSL_RAW_DATA, $iv));
	        $encodedIV = base64_encode($iv);
	        $encryptedPayload = $encodedEncryptedData.":".$encodedIV;
	        
	        return $encryptedPayload;
    	}
    	return NULL;
        
    }
    /**
     * Desencriptar datos cifrados con AES Cipher (CBC) con un llave de 128 bits
     * 
     * @param type $key - La llave debe ser de una longitud exacta de 16 bytes (128 bits)
     * @param type $data - Datos a desencriptar en base 64 junto con el IV separados por :
     * @return Datos desencriptados
     */
    public function decryptData($key, $data) {
        if (strlen($key) == $this->CIPHER_KEY_LEN) {
            $parts = explode(':', $data); //Separate Encrypted data from iv.
        	$decryptedData = openssl_decrypt(base64_decode($parts[0]), $this->OPENSSL_CIPHER_NAME, $key, OPENSSL_RAW_DATA, base64_decode($parts[1]));
        	return $decryptedData;
        }
        return NULL;
    }
    /**
     * Generar una contraseña aleatoria usando un arreglo de carateres y una función de enteros aleatorios
     * 
     * @param type $length longitud de la contraseña retornada.
     * @return contraseña
     */
    public function generate_password($length = 12){
	  $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.
	            '0123456789@=%!*-#()[]{}<>\|~$^&_;:+,./?';
	  $str = '';
	  $max = strlen($chars) - 1;
	  for ($i=0; $i < $length; $i++)
	    $str .= $chars[random_int(0, $max)];
	  return $str;
	}
	/**
     * Generar un llave de encriptación con una función generadora de bytes aleatorios criptográficamente seguros.
     * 
     * @param type $length longitud de bytes .
     * @return bytes criptográficamente seguros
     */
	public function generate_key_encription($length){
		return random_bytes($length);
	}
}