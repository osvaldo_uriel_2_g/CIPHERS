package ciphers;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.security.SecureRandom;
import java.util.Base64;

public class Encrypt {

    private static String CIPHER_NAME = "AES/CBC/PKCS5PADDING";
    private static int CIPHER_KEY_LEN = 16; //128 bits

    /**
     * Encriptar datos usando el cifrado AES Cipher (CBC) con un llave de 128 bits
     * 
     * @param type key - La llave debe ser de una longitud exacta de 16 bytes (128 bits)
     * @param type data - Datos a encriptar
     * @return Datos encriptados y pasados a base64 junto con el IV separados por :
     */
    public static String encrypt(byte[] key, String data) {
        try {
            if (key.length == Encrypt.CIPHER_KEY_LEN) {
            	SecureRandom random = new SecureRandom();
                byte iv[] = new byte[Encrypt.CIPHER_KEY_LEN];
                random.nextBytes(iv);
            	IvParameterSpec initVector = new IvParameterSpec(iv);
                SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

                Cipher cipher = Cipher.getInstance(Encrypt.CIPHER_NAME);
                cipher.init(Cipher.ENCRYPT_MODE, skeySpec, initVector);

                byte[] encryptedData = cipher.doFinal((data.getBytes()));
                
                String base64_EncryptedData = Base64.getEncoder().encodeToString(encryptedData);
                String base64_IV = Base64.getEncoder().encodeToString(iv);
                
                return base64_EncryptedData + ":" + base64_IV;
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /**
     * Desencriptar datos cifrados con AES Cipher (CBC) con un llave de 128 bits
     * 
     * @param type key - La llave debe ser de una longitud exacta de 16 bytes (128 bits)
     * @param type data - Datos a desencriptar en base 64 junto con el IV separados por :
     * @return Datos desencriptados
     */
    
    public static String decrypt(byte[] key, String data) {
        try {
        	if (key.length == Encrypt.CIPHER_KEY_LEN) {
        		String[] parts = data.split(":");
                
                IvParameterSpec iv = new IvParameterSpec(Base64.getDecoder().decode(parts[1]));
                SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

                Cipher cipher = Cipher.getInstance(Encrypt.CIPHER_NAME);
                cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

                byte[] decodedEncryptedData = Base64.getDecoder().decode(parts[0]);

                byte[] original = cipher.doFinal(decodedEncryptedData);

                return new String(original);
        	}
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

}
