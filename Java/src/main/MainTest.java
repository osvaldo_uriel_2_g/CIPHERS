/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import static ciphers.Encrypt.decrypt;
import static ciphers.Encrypt.encrypt;

import java.util.Base64;

/**
 *
 * @author abc1986
 */
public class MainTest {

    public static void main(String[] args) {
    	String key_base64 = "auH8xLEXjZ4h3QbEFjkp+A==";//La misma que en PHP
    	byte[] key = Base64.getDecoder().decode(key_base64);//Hay que decodificar  la llave que esta en base 64
    	String super_secret_message = ":v";
    	String encrypted_message_base64 = encrypt(key, super_secret_message);
    	String decrypted_message = decrypt(key, encrypted_message_base64);
    	System.out.println("Mensaje sin encriptar:");
    	System.out.println(super_secret_message);
    	System.out.println("Mensaje encriptado y pasado a base64 junto con su iv respectivo:");
    	System.out.println(encrypted_message_base64);
    	System.out.println("Mensaje desencriptado:");
    	System.out.println(decrypted_message);
    }
}
